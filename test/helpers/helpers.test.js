import {addOne, getListOfFruitsWithAdjustedPrice} from "../../src/helpers/helpers";

describe("Actions", () => {
  describe(".addOne", () => {
    it("should equal 10 when input 9", () => {
      const result = addOne(9);

      expect(result).toEqual(10);
    });

    it("should equal 11 when input 9", () => {
      const result = addOne(9);

      expect(result).toEqual(11);
    });
  });

  describe(".getListOfFruitsWithAdjustedPrice", () => {
    it("should return a list with prices multiplied by input", () => {
      const list = getListOfFruitsWithAdjustedPrice(10);

      expect(list).toEqual([{"name": "lemon", "price": 1230}, {"name": "apple", "price": 360}, {"name": "kiwi", "price": 990}]);
    });

    it("should return a snapshot", () => {
      const list = getListOfFruitsWithAdjustedPrice(10);

      expect(list).toMatchSnapshot();
    });
  });
});
