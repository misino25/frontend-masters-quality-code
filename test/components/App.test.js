import React from "react";
import {mount, shallow} from "enzyme";
import App from "../../src/components/App";
import * as Helpers from "../../src/helpers/helpers";

describe("App component", () => {
  it("should contain title", () => {
    const component = mount(<App />);
    const titleText = component.find("h1.App-title").text();

    expect(titleText).toEqual("Welcome to React");
  });

  it("should contain title (using shallow component)", () => {
    const component = shallow(<App />);
    const titleText = component.find("h1.App-title").text();

    expect(titleText).toEqual("Welcome to React");
  });

  it("should contain title (using mount component)", () => {
    const component = mount(<App />);

    expect(component.html()).toMatchSnapshot();
  });

  it("should call addOne on click", () => {
    const spy = jest.spyOn(Helpers, "addOne");
    const component = mount(<App />);

    component.find("button").simulate("click");

    expect(spy).toBeCalledWith(0);
  });

  it("should call addOne on click - shallow", () => {
    const spy = jest.spyOn(Helpers, "addOne");
    const component = shallow(<App />);

    component.find("button").simulate("click");

    expect(spy).toBeCalledWith(0);
    component.find("button").simulate("click");
    expect(spy).toBeCalledWith(1);
  });

  describe("show beforeEach/beforeAll example", () => {
    let component;
    beforeEach(() => {
      component = mount(<App />);
    });
    it("should contain title", () => {
      const titleText = component.find("h1.App-title").text();

      expect(titleText).toEqual("Welcome to React");
    });

    it("should contain intro text", () => {
      expect(component.find("p.App-intro").text()).toEqual("How are you today, Pluto.");
    });

    it("should contain button", () => {
      expect(component.find("button").length).toEqual(1);
    });
  });
});
