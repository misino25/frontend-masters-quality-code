// @flow

type Color = "RED" | "BLUE" | "WHITE";
type Fruit = {name: string, price?: number, color?: Color};

export function addOne(input: number) {
  return input + 1;
}

export function getListOfFruitsWithAdjustedPrice(currencyRate: number): Array<Fruit> {
  const fruits = [
    {name: "lemon", price: 123},
    {name: "blueberry", price: 36, color: "BLUE"},
    {name: "kiwi", price: 99},
  ];

  return fruits.map((fruit: Fruit) => ({
    name: fruit.name,
    price: (fruit.price ? fruit.price : 0) * currencyRate,
  }));
}
