// @flow
import React from "react";
import logo from "../styles/logo.svg";
import "../styles/App.css";
import {addOne, getListOfFruitsWithAdjustedPrice} from "../helpers/helpers";

type IntroProps = {
  name: string,
};

type State = {count: number};

const Intro = (props: IntroProps) => (
  <p className="App-intro">
    How are you today, <string>{props.name}</string>.
  </p>
);

class App extends React.Component<{}, State> {
  state = {count: 0};

  addOne = () => {
    const newCount = addOne(this.state.count);
    this.setState({
      count: newCount,
    });
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Intro name="Pluto" />
        <div className="counter">{this.state.count}</div>
        <button onClick={this.addOne}>Add one</button>

        <div>
          <ul>
            {getListOfFruitsWithAdjustedPrice(0.3).map(fruit => (
              <li key={fruit.name}>
                {fruit.name} costs {fruit.price} {fruit.color}
              </li>
            ))}
          </ul>
        </div>
      </div>
    );
  }
}

export default App;
