import React from "react";
import ReactDOM from "react-dom";
import "./styles/index.css";
import App from "./components/App";

// eslint-disable-next-line
ReactDOM.render(<App />, document.getElementById('root'));
