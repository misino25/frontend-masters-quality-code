For all commands, you can use `npm` or `yarn` package manager. It is up to you which you want to choose.
# Installation
`yarn` or `npm install`

# Start application
`yarn start` or `npm start`

# Running Eslint
Get help: `yarn eslint --help`

Check styles: `yarn eslint ./src`

Check styles and automatically fix errors: `yarn eslint ./src --fix`

See current configuration of eslint for given file: `yarn eslint --print-config ./`

# Running static type checking - Flow
Get help: `yarn flow --help`

Run checking once: `yarn flow` or `npm flow`

Run checking with running flow server (it is faster): `yarn flow status`

# Running tests
Get help: `yarn test --help`

Run all tests: `yarn test` or `npm test`

Run tests in path: `yarn test ./test/helpers`

Generate test coverage report: `yarn test --coverage`
